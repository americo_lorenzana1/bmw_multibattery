#include <CANIncludes.h>

//given in VoltsDC
#define LOGICHIGHVOLTAGE        3
#define LOGICLOWVOLTAGE         0
//given in milliseconds
#define IGNITIONPULSELENGTH     500
//Network managment timeout
#define NETWORKMANAGMENTTIME    1920
//Error Timeout
#define ERRORTIMEOUT            5000
//CAT6 Contactor Closing Timeout
#define CONTACTORCLOSETIME      1980

//APP IDs
#define CRC_SPEC_DVSW_HVSTO_APP_ID 85
#define CRC_ST_HVSTO_VRFD_APP_ID 1193
#define CRC_CTR_CR_APP_ID 251

//Cell temps
#define         ROOMTEMPERATURE 23
#define         BELOWZERO       -5
#define         HIGHRANGE       33
#define         CRITICAL        42

const unsigned char crcLookUp[256]=
{
    0x00, 0x1d, 0x3a, 0x27, 0x74, 0x69, 0x4e, 0x53,
    0xe8, 0xf5, 0xd2, 0xcf, 0x9c, 0x81, 0xa6, 0xbb,
    0xcd, 0xd0, 0xf7, 0xea, 0xb9, 0xa4, 0x83, 0x9e,
    0x25, 0x38, 0x1f, 0x02, 0x51, 0x4c, 0x6b, 0x76,
    0x87, 0x9a, 0xbd, 0xa0, 0xf3, 0xee, 0xc9, 0xd4,
    0x6f, 0x72, 0x55, 0x48, 0x1b, 0x06, 0x21, 0x3c,
    0x4a, 0x57, 0x70, 0x6d, 0x3e, 0x23, 0x04, 0x19,
    0xa2, 0xbf, 0x98, 0x85, 0xd6, 0xcb, 0xec, 0xf1,
    0x13, 0x0e, 0x29, 0x34, 0x67, 0x7a, 0x5d, 0x40,
    0xfb, 0xe6, 0xc1, 0xdc, 0x8f, 0x92, 0xb5, 0xa8,
    0xde, 0xc3, 0xe4, 0xf9, 0xaa, 0xb7, 0x90, 0x8d,
    0x36, 0x2b, 0x0c, 0x11, 0x42, 0x5f, 0x78, 0x65,
    0x94, 0x89, 0xae, 0xb3, 0xe0, 0xfd, 0xda, 0xc7,
    0x7c, 0x61, 0x46, 0x5b, 0x08, 0x15, 0x32, 0x2f,
    0x59, 0x44, 0x63, 0x7e, 0x2d, 0x30, 0x17, 0x0a,
    0xb1, 0xac, 0x8b, 0x96, 0xc5, 0xd8, 0xff, 0xe2,
    0x26, 0x3b, 0x1c, 0x01, 0x52, 0x4f, 0x68, 0x75,
    0xce, 0xd3, 0xf4, 0xe9, 0xba, 0xa7, 0x80, 0x9d,
    0xeb, 0xf6, 0xd1, 0xcc, 0x9f, 0x82, 0xa5, 0xb8,
    0x03, 0x1e, 0x39, 0x24, 0x77, 0x6a, 0x4d, 0x50,
    0xa1, 0xbc, 0x9b, 0x86, 0xd5, 0xc8, 0xef, 0xf2,
    0x49, 0x54, 0x73, 0x6e, 0x3d, 0x20, 0x07, 0x1a,
    0x6c, 0x71, 0x56, 0x4b, 0x18, 0x05, 0x22, 0x3f,
    0x84, 0x99, 0xbe, 0xa3, 0xf0, 0xed, 0xca, 0xd7,
    0x35, 0x28, 0x0f, 0x12, 0x41, 0x5c, 0x7b, 0x66,
    0xdd, 0xc0, 0xe7, 0xfa, 0xa9, 0xb4, 0x93, 0x8e,
    0xf8, 0xe5, 0xc2, 0xdf, 0x8c, 0x91, 0xb6, 0xab,
    0x10, 0x0d, 0x2a, 0x37, 0x64, 0x79, 0x5e, 0x43,
    0xb2, 0xaf, 0x88, 0x95, 0xc6, 0xdb, 0xfc, 0xe1,
    0x5a, 0x47, 0x60, 0x7d, 0x2e, 0x33, 0x14, 0x09,
    0x7f, 0x62, 0x45, 0x58, 0x0b, 0x16, 0x31, 0x2c,
    0x97, 0x8a, 0xad, 0xb0, 0xe3, 0xfe, 0xd9, 0xc4
};

typedef struct
{
    DIAG_OBD_ENGMG_EL   diag_obd_engmg_el;
    CHGNG_ST            chgng_st;
    DT_PT_2             dt_pt_2;
    SPEC_HVSTO          spec_hvsto;
    AVL_DT_LE           avl_dt_le;
    SPEC_DCSW_HVSTO     spec_dcsw_hvsto;
    KLEMMEN             klemmen;
    CTR_CR              ctr_cr;
    CTR_PRTNT           ctr_prtnt;
    V_VEH               v_veh;
    HT_MGT_ENG_CTR      ht_mgt_eng_ctr;
    A_TEMP              a_temp;
    STAT_ENG_STA_AUTO   stat_eng_sta_auto;
    RELATIVZEIT         relativzeit;
    KILOMETERSTAND      kilometerstand;
    RLS_COOL_HVSTO      rls_cool_hvsto;
    FAHRGESTELLNUMMER   fahrgestellnummer;
    DIAG_OBD_ENG        diag_obd_eng;
    FZZSTD              fzzstd;
    FRC_RD_INFO         frc_rd_info;
    SVC_VCU             svc_vcu;
    NM2_A_CAN_VCU       nm2_a_can_vcu;
}PacketIdRxT;

typedef struct
{
    ST_CHG_HVSTO_1      st_chg_hvsto_1;
    ST_OPMO_HYB_2       st_opmo_hyb_2;
    DT_HVSTO            dt_hvsto;
    MOD_VC              mod_vc;
    STAT_HVSTO_2        stat_hvsto_2;
    ST_HVSTO_1          st_hvsto_1;
    DT_HVSTO_2          dt_hvsto_2;
    STAT_HVSTO_VRFD     stat_hvsto_vrfd;
    LIM_CHG_DCHG_HVSTO  lim_chg_dchg_hvsto;
    IDENT_HVSTO         ident_hvsto;
    SVC_SME             svc_sme;
    ST_DIAG_OBD_2_PT    st_diag_obd_2_pt;
    NM2_A_CAN_SME       nm2_a_can_sme;
}PacketIdTxT;

// typedef enum
// {
//     VEHICLEOFF,
//     BALANCING,
//     STANDBY,    //KL30 on/active
//     ACTIVE      // KL15 on/active
// }BMWStates;

typedef struct
{
    unsigned char   cluster;
    unsigned char   BattId;
    unsigned char   currentState;
    unsigned char   nextState;
    bool            networkManagement;
    bool            nm_timeout;
    int             networkManagementCounter;
    bool            flag3;
    bool            flag5;
    bool            flag7;
    bool            flag8;
    bool            flag9;
    uint8_t         stateControl;;
    uint8_t         CAT7error;
    uint8_t         CAT6error;
    uint8_t         CAT5error;
    uint8_t         CAT4error;
    uint8_t         CAT3error;
    uint8_t         CAT1error;
    bool            errorStatus;
    bool            errorFlag;
    bool            startCAT6Timer;
    bool            startCAT5Timer;
    int             errorTransitionTime;
    int             errorTime;
    uint8_t         contactorStatus;
    uint16_t        BattVoltage;
    int16_t         BattCurrent;
    int16_t         cellTemperature;
    int16_t         cellTemperatureMax;
    int16_t         cellTemperatureMin;
    uint8_t         RLS_VCU;
    bool            coolingFlag;
    uint8_t         valveCommand;
    uint16_t        batteryIsoResistance;
    uint8_t         displayedSoC;
    int16_t         DCLinkVoltage;
    int             coolingDemand;
    int             coolingStatus;
    uint8_t         expansionValveStatus;
    int             tempDerate;
    uint8_t         hvilFailure;
    int             smeNetworkManagementCounter;
}variableT;

class i3
{
public:
    i3(unsigned char cluster, unsigned char BattId );
    ~i3() { };
    
    //utilities
    bool IsTxActive();
    void print();
    void ControlLoop();
    void priorState() {
        //Decrease state
        switch(var.currentState)
        {
            case BALANCING:
                var.nextState = VEHICLEOFF;
            break;

            case STANDBY:
                var.nextState = BALANCING;
            break;

            case ACTIVE:
                var.nextState = STANDBY;
            break;

            default:
                Trace("Next State is Invalid\n");
            break;
        }
    };
    void postState() {
        //Increase state
        switch(var.currentState)
        {
            case VEHICLEOFF:
                var.nextState = BALANCING;
            break;

            case BALANCING:
                var.nextState = STANDBY;
            break;

            case STANDBY:
                var.nextState = ACTIVE;
            break;

            default:
                Trace("Next State is Invalid\n");
        }
    };

    void printState(){
        //If network should be enabled and it is disabled then report network failure
        if(var.networkManagement == false && ((var.currentState == STANDBY) || (var.currentState == ACTIVE)))
        {
            Trace("Ch %d, BattId %d: Network management failure", var.cluster, var.BattId);
        }

        //Report the state
        switch(var.currentState)
        {
            case VEHICLEOFF:
                Trace("Ch %d, BattId %d: In Off State", var.cluster, var.BattId);
            break;

            case BALANCING:
                Trace("Ch %d, BattId %d: In BALANCING State", var.cluster, var.BattId);
            break;

            case STANDBY:
                Trace("Ch %d, BattId %d: In STANDBY State", var.cluster, var.BattId);
            break;

            case ACTIVE:
                Trace("Ch %d, BattId %d: In Active State", var.cluster, var.BattId);
            break;
        }
    }
    void CAT6timer() {
        if (var.startCAT6Timer){
            if(var.errorTime) var.errorTime--;  
        }
    };
    void CAT5timer() {
        if (var.startCAT5Timer){
            if(var.errorTransitionTime) var.errorTransitionTime--;
        }
    };
    void NetworkMngmt() {
        if (var.networkManagementCounter){
            var.networkManagementCounter--;
        }else{
            var.nm_timeout = true;
        }
    };

    //functions to update tx messages
    void st_chg_hvsto_1();
    void st_opmo_hyb_2();
    void dt_hvsto();
    void mod_vc();
    void stat_hvsto_2();
    void st_hvsto_1();
    void dt_hvsto_2();
    void stat_hvsto_vrfd();
    void lim_chg_dchg_hvsto();
    void ident_hvsto();
    void svc_sme();
    void st_diag_obd_2_pt();
    void nm2_a_can_sme();
    
    
    PacketIdRxT Rx;
    PacketIdTxT Tx;
    variableT   var;

    enum{VEHICLEOFF, BALANCING, STANDBY, ACTIVE};

private:
    void MsgInit(STCAN_MSG& message){
        message.cluster  = var.cluster;
        message.id      &= ~(3);
        message.id      |= var.BattId;
    };
    unsigned char calculateCrc (STCAN_MSG message, unsigned int appId);

    void ClearFlags (){
       var.flag3 = false;
       var.flag5 = false;
       var.flag7 = false;
       var.flag8 = false;
       var.flag9 = false;
    };

    void OneShotMessages(){
        //STAT_HVSTO_2 - Initial Values
        Tx.stat_hvsto_2.AVL_I_HVSTO.physicalvalue(var.BattCurrent); //Measured current of the high voltage battery in amps
        Tx.stat_hvsto_2.AVL_U_HVSTO.physicalvalue(var.BattVoltage); //measured voltage of the battery in volts.
        Tx.stat_hvsto_2.CHGCOND_HVSTO.physicalvalue(94); //bottom limit of the high voltage battery
        Tx.stat_hvsto_2.RQ_OPC_CHG_HVSTO.physicalvalue(var.CAT5error); //CAT 5 error
        Tx.stat_hvsto_2.RQ_OPN_DCSW_HVSTO_ILY.physicalvalue(var.CAT7error); //CAT 7 error
        Tx.stat_hvsto_2.RQ_OPN_DCSW_HVSTO_FAST.physicalvalue(var.CAT6error); //CAT 6 error
        Tx.stat_hvsto_2.CHGCOND_HVSTO_DELTA.physicalvalue(1); //Difference between two SoC signals
        Tx.stat_hvsto_2.AVL_U_LINK.physicalvalue(var.DCLinkVoltage);
        
        //DT_HVSTO_2 - Initial Values
        //dt_hvsto_2.MUX_DT_HVSTO_2.physicalvalue(15);//Battery mode
        //dt_hvsto_2.PRD_ENERG_CHGCOND_2_COS.physicalvalue(38587);//Amount of energy which is aimed as charging target given in Wh
        Tx.dt_hvsto_2.PRD_ENERG_CHGTAR_2_HVSTO.physicalvalue(2);//amount of energy which has to be supplied to the storage which is aimed as charging target
        //TODO:Add logic here to control this
        Tx.dt_hvsto_2.AVL_ISRE.physicalvalue(var.batteryIsoResistance); //iso resistance of the storage in kOhms

        //LIM_CHG_DCHG_HVSTO
        Tx.lim_chg_dchg_hvsto.U_MAX_CHG_HVSTO.physicalvalue(402.4); //max permitted charging voltage
        Tx.lim_chg_dchg_hvsto.I_DYN_MAX_CHG_HVSTO.physicalvalue(0); //limit charging current of battery
        Tx.lim_chg_dchg_hvsto.U_MIN_DCHG_HVSTO.physicalvalue(259.7); //minimum discharge voltage
        Tx.lim_chg_dchg_hvsto.I_DYN_MAX_DCHG_HVSTO.physicalvalue(0); //discharge current limit of the battery
        
        //STAT_HVSTO_VRFD
        //TODO:Add logic here to control this, watch out for CRC race condition
        Tx.stat_hvsto_vrfd.AVL_I_HVSTO_VRFD.physicalvalue(0.3);
        
        //ST_OPMI_HYB_2
        Tx.st_opmo_hyb_2.ST_COOL_HVSTO.physicalvalue(0); //Error status of the storage tank cooling
        
        //DT_HVSTO
        Tx.dt_hvsto.ST_SER_DSCO_PLG.physicalvalue(2);
        Tx.dt_hvsto.ST_MEASMT_ISL.physicalvalue(1); //Signal contains the status of iso measurement
        //control the request for stopping charging.
        Tx.dt_hvsto.RQ_ABRT_CHGNG.physicalvalue(0); // Demand to interrupt the charging process by SME
        Tx.dt_hvsto.PRD_DUR_CHGNG.physicalvalue(65535); //Predicted charging time allowed in minutes
        Tx.dt_hvsto.PRD_T_EOCHG.physicalvalue(1275); //Predicted time until the charging ends
        Tx.dt_hvsto.ENC_HVSTO_MAX.physicalvalue(41.78); //max possible energy content of battery
        
        //MOD_VC
        Tx.mod_vc.RQ_CHGCOND_HVSTO_MIN.physicalvalue(11.5); //Signal contains the bottom limit (SoC-limit)
        Tx.mod_vc.RQ_CHGCOND_HVSTO_MAX.physicalvalue(100); //Signal contains the upper limit (SoC-limit)
        Tx.mod_vc.DISP_AVL_CHGCOND_HVSTO.physicalvalue(var.displayedSoC); //Signal contains the SoC of the high voltage battery which is shown to the driver.
        Tx.mod_vc.ST_STOR_AIC_COOL_RQMT.physicalvalue(0); //Contains the cooling request in watts
        
        //ST_HVSTO_1
        Tx.st_hvsto_1.ST_ERR_ISL_EXTN_BN_HV.physicalvalue(1); //error information of the external iso measurement
        Tx.st_hvsto_1.ST_ERR_ISL_INTL_BN_HV.physicalvalue(1); //error information of the internal iso measurement
        //Control based on temperature
        Tx.st_hvsto_1.RQ_COOL_HVSTO.physicalvalue(var.coolingDemand); //This signal contains a demand to enable cooling
        Tx.st_hvsto_1.ST_VA_COOL_HVSTO.physicalvalue(var.coolingStatus); //Contains the status of the cooling valve of the battery
        Tx.st_hvsto_1.ST_ERR_LOKG_HVSTO.physicalvalue(var.hvilFailure); //Error status of the HVIL
        //TODO:Add logic here to control this
        Tx.st_hvsto_1.ST_PRCHRG_LOKD_HVSTO.physicalvalue(var.CAT1error); //status of the lock of precharge/CAT 1 error
        //precharge active or not
        Tx.st_hvsto_1.ST_DCSW_HVSTO.physicalvalue(0); //Status of the contactors
        //contactorStatus = 0;
        //Limp home disabled
        Tx.st_hvsto_1.ST_EMMOD_HVSTO.physicalvalue(1); //Status of limp home mode
        Tx.st_hvsto_1.RQ_SER_HVSTO.physicalvalue(var.CAT3error); //CAT 3 error
        Tx.st_hvsto_1.ERR_EMMOD_HVSTO.physicalvalue(var.CAT4error); //CAT 4 error
        //TODO:Add logic here to control this
        Tx.st_hvsto_1.ST_ERR_DCSW_HVSTO.physicalvalue(0); //status of the welding contactors
        //resistance ok
        Tx.st_hvsto_1.ST_WARN_ISL_BN_HV.physicalvalue(1);//value of the insulation resistance below the warning limit
        Tx.st_hvsto_1.ST_CSOV_HVSTO.physicalvalue(var.expansionValveStatus); //status of the cooling valve before the battery.
        //Set the temperature on button press
        Tx.st_hvsto_1.AVL_TEMP_HVSTO.physicalvalue(var.cellTemperature); //Average cell temperature of the cells
        Tx.st_hvsto_1.AVL_TEMP_HTEX_HVSTO.physicalvalue(25); //temperature of the cooling element of the battery
        Tx.st_hvsto_1.AVL_TEMP_HVSTO_MIN.physicalvalue(var.cellTemperatureMin); //minimum cell temperature
        Tx.st_hvsto_1.AVL_TEMP_HVSTO_MAX.physicalvalue(var.cellTemperatureMax); //max cell temperature
        //Temperature derate logic
        Tx.st_hvsto_1.ST_WARN_OTMP_HVSTO.physicalvalue(var.tempDerate); //temperature derate active
        
        //IDENT_HVSTO
        Tx.ident_hvsto.IDENT_HVSTO_SIG.physicalvalue(1337); //serial number of the battery
        
        //ST_CHG_HVSTO_1
        Tx.st_chg_hvsto_1.AVLB_PWR_SRT_CHG_HVSTO.physicalvalue(0); // Charging capacity over 1 second
        Tx.st_chg_hvsto_1.AVLB_PWR_SRT_DCHG_HVSTO.physicalvalue(0); // Discharging capacity over 1 second
        Tx.st_chg_hvsto_1.AVLB_PWR_LT_CHG_HVSTO.physicalvalue(0); //Charging capacity over 5 seconds
        Tx.st_chg_hvsto_1.AVLB_PWR_LT_DCHG_HVSTO.physicalvalue(0); //Discharging capacity over 5 seconds   

    };

    struct Counter4b{
       uint8_t val      :4;
       uint8_t          :4; 
    };

};

i3::i3(unsigned char cluster, unsigned char BattId )
{
    std::memset(&var, 0, sizeof(var));

    var.cluster  = cluster;
    var.BattId   = BattId;
    var.smeNetworkManagementCounter = 3;

    MsgInit(Tx.st_chg_hvsto_1);
    MsgInit(Tx.st_opmo_hyb_2);
    MsgInit(Tx.dt_hvsto);
    MsgInit(Tx.mod_vc);
    MsgInit(Tx.stat_hvsto_2);
    MsgInit(Tx.st_hvsto_1);
    MsgInit(Tx.dt_hvsto_2);
    MsgInit(Tx.stat_hvsto_vrfd);
    MsgInit(Tx.lim_chg_dchg_hvsto);
    MsgInit(Tx.ident_hvsto);
    MsgInit(Tx.svc_sme);
    MsgInit(Tx.st_diag_obd_2_pt);
    MsgInit(Tx.nm2_a_can_sme);

    Tx.nm2_a_can_sme.NM2_USR_DT_ACAN.rawvalue(0);
    Tx.nm2_a_can_sme.NM2_AUTOSAR_RES_ACAN.rawvalue(0);
    Tx.nm2_a_can_sme.data[7] = 0x07;
}

unsigned char i3::calculateCrc (STCAN_MSG message, unsigned int appId)
{
    unsigned char crcSum = 0;
    unsigned char lowbyteappid = appId;
    unsigned char highbyteappid = (appId >> 8);
    //get the initial value from the crcLookUp Table
    crcSum = crcLookUp[lowbyteappid];
    //mutation
    crcSum = crcLookUp[(crcSum ^ highbyteappid)];
    //mutation of the following bytes, not including byte 0
    for(int i = 1; i < message.dlc; i++)
    {
        crcSum = crcLookUp[(crcSum ^ message.data[i])];
    }
    return crcSum;
}

void i3::ControlLoop()
{
    if(var.nextState != var.currentState)
    {
        switch (var.nextState)
        {
            case VEHICLEOFF:
                if(var.currentState == BALANCING)
                {
                    var.currentState = VEHICLEOFF;
                    //Clear CAT5, CAT6 and CAT7 Error (Locked)
                    var.CAT7error = 1;
                    var.CAT6error = 1;
                    var.CAT5error = 1;
                    var.CAT1error = 1;
                    var.errorFlag = false;
                }
            break;

            case BALANCING:
                if(var.currentState == VEHICLEOFF)
                {
                    var.currentState = BALANCING;
                }else if(var.currentState == STANDBY)
                {
                    var.currentState = BALANCING;
                }
            break;

            case STANDBY:
                if(var.currentState == BALANCING)
                {
                    var.currentState = STANDBY;
                    var.networkManagement = true;
                    OneShotMessages();
                }else if(var.currentState == ACTIVE)
                {
                    var.currentState = STANDBY;
                    //OneShotMessages();
                }
            break;

            case ACTIVE:
                if(var.currentState == STANDBY)
                {
                    var.currentState = ACTIVE;
                }
            break;

            default:
            break;


        }
    }

    
    //State Control through VCU (KLEMMEN Message)
    if(Rx.klemmen.ST_KL.rawvalue() == 0x03){
        var.flag3 = true;
    } else if(Rx.klemmen.ST_KL.rawvalue() == 0x05){
        var.flag5 = true;
    } else if(Rx.klemmen.ST_KL.rawvalue() == 0x07){
        var.flag7 = true;
    } else if(Rx.klemmen.ST_KL.rawvalue() == 0x08){
        var.flag8 = true;
    } else if(Rx.klemmen.ST_KL.rawvalue() == 0x09){
        var.flag9 = true;
    }
    
    var.stateControl = Rx.klemmen.ST_KL.rawvalue();
    if((var.currentState == VEHICLEOFF || var.currentState == STANDBY) && (var.flag3 == true || var.flag5 == true)){
        if(var.stateControl == 0x04){
            var.nextState = BALANCING;
            ClearFlags();
        }
    }else if(var.currentState == BALANCING && var.flag5 == true){
        if(var.stateControl == 0x06){
            var.nextState = STANDBY;
            ClearFlags();
        }
    }else if(var.currentState == ACTIVE && var.flag9 == true){
        if(var.flag8 == true){
            if(var.flag7 == true){
                if(var.stateControl == 0x06){
                    var.nextState = STANDBY;
                    ClearFlags();
                }
            }
        }
    }else if(var.currentState == STANDBY && var.flag7 == true){
        if(var.flag8 == true){
            if(var.flag9 == true){
                if(var.stateControl == 0x0A){
                    var.nextState = ACTIVE;
                    ClearFlags();
                }
            }
        }
    }else if(var.currentState == BALANCING && var.flag3 == true){
        if(var.stateControl == 0x02){
            var.nextState = VEHICLEOFF;
            ClearFlags();
        }
    }
    
    //Error Categories
    //CAT7
    if(var.CAT7error == 2){
        var.errorStatus = true;
    }
    
    //CAT6
    if(var.CAT6error == 2){
        var.startCAT6Timer = true;
        if(var.errorTime == 0){
            var.errorStatus = true;
        }
    } else {
        var.startCAT6Timer = false;
        var.errorTime = CONTACTORCLOSETIME;
    }
    
    //CAT5
    if(var.CAT5error == 2){
        var.startCAT5Timer = true;
        if(var.errorTransitionTime == 0){
            var.CAT6error = 2;
            var.startCAT5Timer = false;
        }
    } else {
        var.errorTransitionTime = ERRORTIMEOUT;
    }
    
    //CAT1
    if(var.CAT6error == 2 || var.CAT7error == 2){
        var.CAT1error = 2;
    }

    if((var.CAT7error) || (var.CAT6error) || (var.CAT5error) || (var.CAT1error)){
        var.errorStatus = false;
    }
    
    //VCU Commanded Contactor Status
    if((var.CAT1error == 1) && (var.errorFlag == false)){
        if(var.cellTemperature <= 50){
            var.contactorStatus = Rx.spec_dcsw_hvsto.RQ_DCSW_HVSTO_CLO.physicalvalue();
        }
    }
    
    //Cooling Release Signal
    var.RLS_VCU = Rx.rls_cool_hvsto.RLS_COOL_HVSTO_SIG.physicalvalue();
    
    //Expansion Valve Control (VCU commanded)
    var.valveCommand = Rx.rls_cool_hvsto.RQ_CSOV_HVSTO.physicalvalue();
    
    //DC-Link Voltage Logic
    // if(parallelMode){
        // --For Parallel Connection--
        if((var.contactorStatus == 1))
        {
            while(var.DCLinkVoltage != var.BattVoltage){
                //DCLinkVoltage = storedData[i].BattVoltage;
                var.DCLinkVoltage++;
            }
        } else {
            var.DCLinkVoltage = 0;
        }
    // } else {
    //     // --For Connected Connection--
    //     if((var.contactorStatus == 1))
    //     {
    //         channelOff = 0;
    //         if(DCLinkVolt < var.BattVoltage)
    //         {
    //             DCLinkVolt = var.BattVoltage;
    //         } else {
    //             int a = var.BattVoltage - DCLinkVolt;
    //             if((a < -4) || (a > 0)){
    //                 var.errorFlag = true;
    //             }
    //         }
    //     } else {
    //         //stat_hvsto_2.AVL_U_LINK.physicalvalue(0);
    //         //storedData[i].contactorStatus = 0;
    //         channelOff++;
    //     }
    //     if(channelOff == BATTERY_NUMBER){
    //         DCLinkVolt = 0;
    //     }
    // }
    
    //Cooling Request Logic
    if((var.cellTemperature > 31) && (var.cellTemperature < 38))
    {
        var.coolingDemand = 1; //Cooling demanded
        var.coolingFlag = true;
        
        if(var.RLS_VCU == 1)
        {
            var.coolingStatus = 2;
            var.expansionValveStatus = 0;
        }
    } else if (var.cellTemperature >= 38){
        var.coolingDemand = 2; //Cooling required urgently
        
        if(var.RLS_VCU == 1)
        {
            var.coolingStatus = 2;
            var.expansionValveStatus = 0;
        }
    } else {
        var.coolingDemand = 0; //No cooling demanded
        var.coolingStatus = 1; //Contains the status of the cooling valve of the battery
        var.coolingFlag =  false;
    }
    
    if(var.cellTemperature > 50)
    {
        //st_hvsto_1.ST_DCSW_HVSTO.physicalvalue(0); //Open Contactors
        var.contactorStatus = 0;
    }

    if(var.coolingFlag == false)
    {
        if(var.valveCommand == 1)
        {
            var.coolingStatus = 2;
            var.expansionValveStatus = 0;
        } else if (var.valveCommand == 0) {
            var.coolingStatus = 1;
        }
    }
    
    //Temperature derate logic
    if(var.cellTemperature >= 38)
    {
        //Too hot, derate
        var.tempDerate = 2;
    }else if(var.cellTemperature <= 0)
    {
        //Too cold, derate
        var.tempDerate = 2;
    }else
    {
        //Nominal range, do not derate
        var.tempDerate = 1; //temperature derate active
    }
}

void i3::st_chg_hvsto_1()
{
    if(IsTxActive())
    {
        SendMsg (Tx.st_chg_hvsto_1);
    }
}

void i3::st_opmo_hyb_2()
{
    if(IsTxActive())
    {
        SendMsg (Tx.st_opmo_hyb_2);
    }
}

void i3::dt_hvsto()
{
    if(IsTxActive())
    {
        SendMsg (Tx.dt_hvsto);
    }
}

void i3::mod_vc()
{
    if(IsTxActive())
    {
        //Cooling amount in Watt
        if(var.coolingFlag ==  true)
        {
            uint16_t coolingAmount = var.cellTemperature * 40;
            if(coolingAmount <= 2000)
            {
                Tx.mod_vc.ST_STOR_AIC_COOL_RQMT.physicalvalue(coolingAmount); //Contains the cooling request in watts
            } else {
                Tx.mod_vc.ST_STOR_AIC_COOL_RQMT.physicalvalue(2000);
            }
        } else {
            Tx.mod_vc.ST_STOR_AIC_COOL_RQMT.physicalvalue(0);
        }

        Tx.mod_vc.DISP_AVL_CHGCOND_HVSTO.physicalvalue(var.displayedSoC); //Signal contains the SoC of the high voltage battery which is shown to the driver.         
        SendMsg (Tx.mod_vc);
    }
}

void i3::stat_hvsto_2()
{
    if(IsTxActive())
    {
        Tx.stat_hvsto_2.AVL_U_HVSTO.physicalvalue(var.BattVoltage); //Measured voltage of the battery in
        Tx.stat_hvsto_2.RQ_OPN_DCSW_HVSTO_ILY.physicalvalue(var.CAT7error); //CAT 7 error
        Tx.stat_hvsto_2.RQ_OPN_DCSW_HVSTO_FAST.physicalvalue(var.CAT6error); //CAT 6 error
        Tx.stat_hvsto_2.RQ_OPC_CHG_HVSTO.physicalvalue(var.CAT5error); //CAT 5 error
        Tx.stat_hvsto_2.AVL_U_LINK.physicalvalue(var.DCLinkVoltage);

        if(var.nm_timeout == true){
            Tx.stat_hvsto_2.AVL_U_LINK.physicalvalue(0);
        }

        SendMsg (Tx.stat_hvsto_2);
    }
}

void i3::st_hvsto_1()
{
    if(IsTxActive())
    {
        Tx.st_hvsto_1.ERR_EMMOD_HVSTO.physicalvalue(var.CAT4error); //CAT 4 error
        Tx.st_hvsto_1.ST_PRCHRG_LOKD_HVSTO.physicalvalue(var.CAT1error); //status of the lock of precharge/CAT 1 error
        //Battery Temperature
        Tx.st_hvsto_1.AVL_TEMP_HVSTO.physicalvalue(var.cellTemperature); //Average cell temperature of the cells
        Tx.st_hvsto_1.AVL_TEMP_HVSTO_MIN.physicalvalue(var.cellTemperatureMin); //minimum cell temperature
        Tx.st_hvsto_1.AVL_TEMP_HVSTO_MAX.physicalvalue(var.cellTemperatureMax); //max cell temperature
        //Temperature derate
        Tx.st_hvsto_1.ST_WARN_OTMP_HVSTO.physicalvalue(var.tempDerate); //temperature derate active  
        //HVIL Failure
        Tx.st_hvsto_1.ST_ERR_LOKG_HVSTO.physicalvalue(var.hvilFailure); //Error status of the HVIL

        if(var.errorFlag == true){
            Tx.st_hvsto_1.ST_PRCHRG_LOKD_HVSTO.physicalvalue(2); //status of the lock of precharge
            var.contactorStatus = 0;
        }
        
        if((var.contactorStatus == 1) && (var.errorStatus == false))
        {
            Tx.st_hvsto_1.ST_DCSW_HVSTO.physicalvalue(2);
        }
        else
        {
            Tx.st_hvsto_1.ST_DCSW_HVSTO.physicalvalue(0);
            var.contactorStatus = 0;
        }

        if(var.batteryIsoResistance < 210)
        {
            //resistance too low
            Tx.st_hvsto_1.ST_WARN_ISL_BN_HV.physicalvalue(2);//value of the insulation resistance below the warning limit
            if(var.contactorStatus == 1){
                Tx.st_hvsto_1.ST_ERR_ISL_EXTN_BN_HV.physicalvalue(2);
            } else {
                Tx.st_hvsto_1.ST_ERR_ISL_INTL_BN_HV.physicalvalue(2);
            }
        }else{
            //resistance ok
            Tx.st_hvsto_1.ST_WARN_ISL_BN_HV.physicalvalue(1);//value of the insulation resistance below the warning limit
            if(var.contactorStatus == 1){
                Tx.st_hvsto_1.ST_ERR_ISL_EXTN_BN_HV.physicalvalue(1);
            } else {
                Tx.st_hvsto_1.ST_ERR_ISL_INTL_BN_HV.physicalvalue(1);
            }
        }

        Tx.st_hvsto_1.RQ_COOL_HVSTO.physicalvalue(var.coolingDemand); //Cooling demanded
        Tx.st_hvsto_1.ST_VA_COOL_HVSTO.physicalvalue(var.coolingStatus); //Contains the status of the cooling valve of the battery
        Tx.st_hvsto_1.ST_CSOV_HVSTO.physicalvalue(var.expansionValveStatus);

        if(var.nm_timeout == true){
        Tx.st_hvsto_1.ST_DCSW_HVSTO.physicalvalue(0);
        }

        if(var.nm_timeout == true){
            var.currentState = i3::VEHICLEOFF;
            var.networkManagement = false;
        }

        SendMsg (Tx.st_hvsto_1);
    }
}

void i3::dt_hvsto_2()
{
    if(IsTxActive())
    {
        struct Counter4b temp = { .val = (uint8_t)Tx.dt_hvsto_2.SYNCN_DT_HVSTO_2.physicalvalue() };
        Tx.dt_hvsto_2.SYNCN_DT_HVSTO_2.physicalvalue(--temp.val);
        Tx.dt_hvsto_2.AVL_ISRE.physicalvalue(var.batteryIsoResistance); //iso resistance of the storage in kOhms
        SendMsg (Tx.dt_hvsto_2);
    }
}

void i3::stat_hvsto_vrfd()
{
    if(IsTxActive())
    {
        //Counter logic
        struct Counter4b temp = { .val = (uint8_t)Tx.stat_hvsto_vrfd.ALIV_ST_HVSTO_VRFD.physicalvalue() };
        Tx.stat_hvsto_vrfd.ALIV_ST_HVSTO_VRFD.physicalvalue(--temp.val);
        
        //CRC calculation
        Tx.stat_hvsto_vrfd.CRC_ST_HVSTO_VRFD.physicalvalue(calculateCrc(Tx.stat_hvsto_vrfd, CRC_ST_HVSTO_VRFD_APP_ID));
        SendMsg (Tx.stat_hvsto_vrfd);
    }
}

void i3::lim_chg_dchg_hvsto()
{
    if(IsTxActive())
    {
        SendMsg (Tx.lim_chg_dchg_hvsto);
    }
}

void i3::ident_hvsto()
{
    if(IsTxActive())
    {
        SendMsg (Tx.ident_hvsto);
    }
}

void i3::svc_sme()
{
    if(IsTxActive())
    {
        SendMsg (Tx.svc_sme);
    }
}

void i3::st_diag_obd_2_pt()
{
    if(IsTxActive())
    {
        struct Counter4b temp = { .val = (uint8_t)Tx.st_diag_obd_2_pt.ALIV_ST_DIAG_OBD_2_PT.physicalvalue() };
        Tx.st_diag_obd_2_pt.ALIV_ST_DIAG_OBD_2_PT.physicalvalue(--temp.val);
        SendMsg (Tx.st_diag_obd_2_pt);
    }
}

void i3::nm2_a_can_sme()
{
    if(IsTxActive())
    {
        //send the network management packet 3 times
        if(var.smeNetworkManagementCounter > 0)
        {
            SendMsg (Tx.nm2_a_can_sme);
            var.smeNetworkManagementCounter--;
        }
    }
}


bool i3::IsTxActive()
{
    return (var.networkManagement == true && ((var.currentState == i3::STANDBY) || (var.currentState == i3::ACTIVE)));
}

void i3::print()
{
    Trace("cluster                                %d ",var.cluster);
    Trace("BattId                                 %d ",var.BattId);
    Trace("currentState                           %d ",var.currentState);
    Trace("nextState                              %d ",var.nextState);
    Trace("networkManagement                      %d ",var.networkManagement);
    Trace("nm_timeout                             %d ",var.nm_timeout);
    Trace("networkManagementCounter               %d ",var.networkManagementCounter);
    Trace("flag3                                  %d ",var.flag3);
    Trace("flag5                                  %d ",var.flag5);
    Trace("flag7                                  %d ",var.flag7);
    Trace("flag8                                  %d ",var.flag8);
    Trace("flag9                                  %d ",var.flag9);
    Trace("stateControl                           %d ",var.stateControl);
    Trace("CAT7error                              %d ",var.CAT7error);
    Trace("CAT6error                              %d ",var.CAT6error);
    Trace("CAT5error                              %d ",var.CAT5error);
    Trace("CAT4error                              %d ",var.CAT4error);
    Trace("CAT3error                              %d ",var.CAT3error);
    Trace("CAT1error                              %d ",var.CAT1error);
    Trace("errorStatus                            %d ",var.errorStatus);
    Trace("errorFlag                              %d ",var.errorFlag);
    Trace("startCAT6Timer                         %d ",var.startCAT6Timer);
    Trace("startCAT5Timer                         %d ",var.startCAT5Timer);
    Trace("errorTransitionTime                    %d ",var.errorTransitionTime);
    Trace("errorTime                              %d ",var.errorTime);
    Trace("contactorStatus                        %d ",var.contactorStatus);
    Trace("BattVoltage                            %d ",var.BattVoltage);
    Trace("BattCurrent                            %d ",var.BattCurrent);
    Trace("cellTemperature                        %d ",var.cellTemperature);
    Trace("cellTemperatureMax                     %d ",var.cellTemperatureMax);
    Trace("cellTemperatureMin                     %d ",var.cellTemperatureMin);
    Trace("RLS_VCU                                %d ",var.RLS_VCU);
    Trace("coolingFlag                            %d ",var.coolingFlag);
    Trace("valveCommand                           %d ",var.valveCommand);
    Trace("batteryIsoResistance                   %d ",var.batteryIsoResistance);
    Trace("displayedSoC                           %d ",var.displayedSoC);
    Trace("DCLinkVoltage                          %d ",var.DCLinkVoltage);
    Trace("coolingDemand                          %d ",var.coolingDemand);
    Trace("coolingStatus                          %d ",var.coolingStatus);
    Trace("expansionValveStatus                   %d ",var.expansionValveStatus);
    Trace("tempDerate                             %d ",var.tempDerate);
    Trace("hvilFailure                            %d ",var.hvilFailure);
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "RQ_RST_OBD_DIAG             %#X ", Rx.diag_obd_engmg_el.RQ_RST_OBD_DIAG.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.chgng_st");
    Trace( "WISH_UTS_ENC                %#X ", Rx.chgng_st.WISH_UTS_ENC.physicalvalue());
    Trace( "RQ_CHGRDI                   %#X ", Rx.chgng_st.RQ_CHGRDI.physicalvalue());
    Trace( "FRC_PWR_CHGNG               %#X ", Rx.chgng_st.FRC_PWR_CHGNG.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.dt_pt_2");
    Trace( "ST_DRV_VEH                  %#X ", Rx.dt_pt_2.ST_DRV_VEH.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "CTR_MEASMT_ISL              %#X ", Rx.spec_hvsto.CTR_MEASMT_ISL.physicalvalue());
    Trace( "ST_HT_HVSTO                 %#X ", Rx.spec_hvsto.ST_HT_HVSTO.physicalvalue());
    Trace( "RQ_EMMOD_HYB_HVSTO          %#X ", Rx.spec_hvsto.RQ_EMMOD_HYB_HVSTO.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "ST_AC_CHGNG_EPRG            %#X ", Rx.avl_dt_le.ST_AC_CHGNG_EPRG.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "CRC_SPEC_DCSW_HVSTO         %#X ", Rx.spec_dcsw_hvsto.CRC_SPEC_DCSW_HVSTO.physicalvalue());
    Trace( "ALIV_SPEC_DCSW_HVSTO        %#X ", Rx.spec_dcsw_hvsto.ALIV_SPEC_DCSW_HVSTO.physicalvalue());
    Trace( "RQ_DCSW_HVSTO_CLO           %#X ", Rx.spec_dcsw_hvsto.RQ_DCSW_HVSTO_CLO.physicalvalue());
    Trace( "ST_DCHG_LINK_2              %#X ", Rx.spec_dcsw_hvsto.ST_DCHG_LINK_2.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "ST_VEH_CON                  %#X ", Rx.klemmen.ST_VEH_CON.physicalvalue());
    Trace( "ST_KL                       %#X ", Rx.klemmen.ST_KL.physicalvalue());
    Trace( "ST_KL_30B                   %#X ", Rx.klemmen.ST_KL_30B.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "CRC_CTR_CR                  %#X ", Rx.ctr_cr.CRC_CTR_CR.physicalvalue());
    Trace( "ALIV_CTR_CR                 %#X ", Rx.ctr_cr.ALIV_CTR_CR.physicalvalue());
    Trace( "ST_EXCE_ACLN_THRV           %#X ", Rx.ctr_cr.ST_EXCE_ACLN_THRV.physicalvalue());
    Trace( "CTR_PHTR_CR                 %#X ", Rx.ctr_cr.CTR_PHTR_CR.physicalvalue());
    Trace( "CTR_PCSH_MST                %#X ", Rx.ctr_cr.CTR_PCSH_MST.physicalvalue());
    Trace( "CTR_HAZW_CR                 %#X ", Rx.ctr_cr.CTR_HAZW_CR.physicalvalue());
    Trace( "CTR_SWO_EKP_CR              %#X ", Rx.ctr_cr.CTR_SWO_EKP_CR.physicalvalue());
    Trace( "CTR_ITLI_CR                 %#X ", Rx.ctr_cr.CTR_ITLI_CR.physicalvalue());
    Trace( "CTR_AUTOM_ECAL_CR           %#X ", Rx.ctr_cr.CTR_AUTOM_ECAL_CR.physicalvalue());
    Trace( "CTR_CLSY_CR                 %#X ", Rx.ctr_cr.CTR_CLSY_CR.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "CTR_BS_PRTNT_DRV            %#X ", Rx.ctr_prtnt.CTR_BS_PRTNT_DRV.physicalvalue());
    Trace( "CTR_FKTN_PRTNT_DRV          %#X ", Rx.ctr_prtnt.CTR_FKTN_PRTNT_DRV.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "V_VEH_COG                   %#X ", Rx.v_veh.V_VEH_COG.physicalvalue());
    Trace( "QU_V_VEH_COG                %#X ", Rx.v_veh.QU_V_VEH_COG.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "ST_BLEED_DME                %#X ", Rx.ht_mgt_eng_ctr.ST_BLEED_DME.physicalvalue());
    Trace( "RQ_SOV_HVSTO                %#X ", Rx.ht_mgt_eng_ctr.RQ_SOV_HVSTO.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "TEMP_EX                     %#X ", Rx.a_temp.TEMP_EX.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "SPEC_TYP_ENGSTA             %#X ", Rx.stat_eng_sta_auto.SPEC_TYP_ENGSTA.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "T_SEC_COU_REL               %#X ", Rx.relativzeit.T_SEC_COU_REL.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "MILE_KM                     %#X ", Rx.kilometerstand.MILE_KM.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "RLS_COOL_HVSTO_SIG          %#X ", Rx.rls_cool_hvsto.RLS_COOL_HVSTO_SIG.physicalvalue());
    Trace( "RQ_PAIC                     %#X ", Rx.rls_cool_hvsto.RQ_PAIC.physicalvalue());
    Trace( "RQ_CSOV_HVSTO               %#X ", Rx.rls_cool_hvsto.RQ_CSOV_HVSTO.physicalvalue());
    Trace( "ST_FRT_AC                   %#X ", Rx.rls_cool_hvsto.ST_FRT_AC.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "NO_VECH_1                   %#X ", Rx.fahrgestellnummer.NO_VECH_1.physicalvalue());
    Trace( "NO_VECH_2                   %#X ", Rx.fahrgestellnummer.NO_VECH_2.physicalvalue());
    Trace( "NO_VECH_3                   %#X ", Rx.fahrgestellnummer.NO_VECH_3.physicalvalue());
    Trace( "NO_VECH_4                   %#X ", Rx.fahrgestellnummer.NO_VECH_4.physicalvalue());
    Trace( "NO_VECH_5                   %#X ", Rx.fahrgestellnummer.NO_VECH_5.physicalvalue());
    Trace( "NO_VECH_6                   %#X ", Rx.fahrgestellnummer.NO_VECH_6.physicalvalue());
    Trace( "NO_VECH_7                   %#X ", Rx.fahrgestellnummer.NO_VECH_7.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "ST_OBD_CYC                  %#X ", Rx.diag_obd_eng.ST_OBD_CYC.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "ST_ENERG_FZM                %#X ", Rx.fzzstd.ST_ENERG_FZM.physicalvalue());
    Trace( "ST_ILK_ERRM_FZM             %#X ", Rx.fzzstd.ST_ILK_ERRM_FZM.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "FRC_AVR_HVSTO_PWR           %#X ", Rx.frc_rd_info.FRC_AVR_HVSTO_PWR.physicalvalue());
    Trace( "SYNCN_FRC_RD_INFO           %#X ", Rx.frc_rd_info.SYNCN_FRC_RD_INFO.physicalvalue());
    Trace( "MUX_FRC_RD_INFO             %#X ", Rx.frc_rd_info.MUX_FRC_RD_INFO.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "ID2                         %#X ", Rx.svc_vcu.ID2.physicalvalue());
    Trace( "RLS_ID_PWRCOS_FN_1          %#X ", Rx.svc_vcu.RLS_ID_PWRCOS_FN_1.physicalvalue());
    Trace( "RLS_PWR_LV_FN_1             %#X ", Rx.svc_vcu.RLS_PWR_LV_FN_1.physicalvalue());
    Trace( "RLS_PWR_HV_FN_1             %#X ", Rx.svc_vcu.RLS_PWR_HV_FN_1.physicalvalue());
    Trace( "RLS_PWR_HV_FN_2             %#X ", Rx.svc_vcu.RLS_PWR_HV_FN_2.physicalvalue());
    Trace( "RLS_PWR_LV_FN_2             %#X ", Rx.svc_vcu.RLS_PWR_LV_FN_2.physicalvalue());
    Trace( "RLS_ID_PWRCOS_FN_2          %#X ", Rx.svc_vcu.RLS_ID_PWRCOS_FN_2.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Rx.diag_obd_engmg_el");
    Trace( "NM2_AUTOSAR_RES_ACAN        %#X ", Rx.nm2_a_can_vcu.NM2_AUTOSAR_RES_ACAN.physicalvalue());
    Trace( "NM2_USR_DT_ACAN             %#X ", Rx.nm2_a_can_vcu.NM2_USR_DT_ACAN.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.st_chg_hvsto_1");
    Trace("AVLB_PWR_SRT_CHG_HVSTO       %#X ", Tx.st_chg_hvsto_1.AVLB_PWR_SRT_CHG_HVSTO.physicalvalue());
    Trace("AVLB_PWR_SRT_DCHG_HVSTO      %#X ", Tx.st_chg_hvsto_1.AVLB_PWR_SRT_DCHG_HVSTO.physicalvalue());
    Trace("AVLB_PWR_LT_CHG_HVSTO        %#X ", Tx.st_chg_hvsto_1.AVLB_PWR_LT_CHG_HVSTO.physicalvalue());
    Trace("AVLB_PWR_LT_DCHG_HVSTO       %#X ", Tx.st_chg_hvsto_1.AVLB_PWR_LT_DCHG_HVSTO.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.st_opmo_hyb_2");
    Trace("ST_COOL_HVSTO                %#X ", Tx.st_opmo_hyb_2.ST_COOL_HVSTO.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.DT_HVSTO");
    Trace("ST_SER_DSCO_PLG              %#X ", Tx.dt_hvsto.ST_SER_DSCO_PLG.physicalvalue());
    Trace("ST_MEASMT_ISL                %#X ", Tx.dt_hvsto.ST_MEASMT_ISL.physicalvalue());
    Trace("RQ_ABRT_CHGNG                %#X ", Tx.dt_hvsto.RQ_ABRT_CHGNG.physicalvalue());
    Trace("PRD_DUR_CHGNG                %#X ", Tx.dt_hvsto.PRD_DUR_CHGNG.physicalvalue());
    Trace("PRD_T_EOCHG                  %#X ", Tx.dt_hvsto.PRD_T_EOCHG.physicalvalue());
    Trace("ENC_HVSTO_MAX                %#X ", Tx.dt_hvsto.ENC_HVSTO_MAX.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.mod_vc");
    Trace("RQ_CHGCOND_HVSTO_MIN         %#X ", Tx.mod_vc.RQ_CHGCOND_HVSTO_MIN.physicalvalue());
    Trace("RQ_CHGCOND_HVSTO_MAX         %#X ", Tx.mod_vc.RQ_CHGCOND_HVSTO_MAX.physicalvalue());
    Trace("DISP_AVL_CHGCOND_HVSTO       %#X ", Tx.mod_vc.DISP_AVL_CHGCOND_HVSTO.physicalvalue());
    Trace("ST_STOR_AIC_COOL_RQMT        %#X ", Tx.mod_vc.ST_STOR_AIC_COOL_RQMT.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.stat_hvsto_2");
    Trace("AVL_I_HVSTO                  %#X ", Tx.stat_hvsto_2.AVL_I_HVSTO.physicalvalue());
    Trace("AVL_U_HVSTO                  %#X ", Tx.stat_hvsto_2.AVL_U_HVSTO.physicalvalue());
    Trace("CHGCOND_HVSTO                %#X ", Tx.stat_hvsto_2.CHGCOND_HVSTO.physicalvalue());
    Trace("RQ_OPC_CHG_HVSTO             %#X ", Tx.stat_hvsto_2.RQ_OPC_CHG_HVSTO.physicalvalue());
    Trace("RQ_OPN_DCSW_HVSTO_ILY        %#X ", Tx.stat_hvsto_2.RQ_OPN_DCSW_HVSTO_ILY.physicalvalue());
    Trace("RQ_OPN_DCSW_HVSTO_FAST       %#X ", Tx.stat_hvsto_2.RQ_OPN_DCSW_HVSTO_FAST.physicalvalue());
    Trace("CHGCOND_HVSTO_DELTA          %#X ", Tx.stat_hvsto_2.CHGCOND_HVSTO_DELTA.physicalvalue());
    Trace("AVL_U_LINK                   %#X ", Tx.stat_hvsto_2.AVL_U_LINK.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.st_hvsto_1");
    Trace("ST_ERR_ISL_EXTN_BN_HV        %#X ", Tx.st_hvsto_1.ST_ERR_ISL_EXTN_BN_HV.physicalvalue());
    Trace("ST_ERR_ISL_INTL_BN_HV        %#X ", Tx.st_hvsto_1.ST_ERR_ISL_INTL_BN_HV.physicalvalue());
    Trace("RQ_COOL_HVSTO                %#X ", Tx.st_hvsto_1.RQ_COOL_HVSTO.physicalvalue());
    Trace("ST_VA_COOL_HVSTO             %#X ", Tx.st_hvsto_1.ST_VA_COOL_HVSTO.physicalvalue());
    Trace("ST_ERR_LOKG_HVSTO            %#X ", Tx.st_hvsto_1.ST_ERR_LOKG_HVSTO.physicalvalue());
    Trace("ST_PRCHRG_LOKD_HVSTO         %#X ", Tx.st_hvsto_1.ST_PRCHRG_LOKD_HVSTO.physicalvalue());
    Trace("ST_DCSW_HVSTO                %#X ", Tx.st_hvsto_1.ST_DCSW_HVSTO.physicalvalue());
    Trace("ST_EMMOD_HVSTO               %#X ", Tx.st_hvsto_1.ST_EMMOD_HVSTO.physicalvalue());
    Trace("RQ_SER_HVSTO                 %#X ", Tx.st_hvsto_1.RQ_SER_HVSTO.physicalvalue());
    Trace("ERR_EMMOD_HVSTO              %#X ", Tx.st_hvsto_1.ERR_EMMOD_HVSTO.physicalvalue());
    Trace("ST_ERR_DCSW_HVSTO            %#X ", Tx.st_hvsto_1.ST_ERR_DCSW_HVSTO.physicalvalue());
    Trace("ST_WARN_ISL_BN_HV            %#X ", Tx.st_hvsto_1.ST_WARN_ISL_BN_HV.physicalvalue());
    Trace("ST_CSOV_HVSTO                %#X ", Tx.st_hvsto_1.ST_CSOV_HVSTO.physicalvalue());
    Trace("AVL_TEMP_HVSTO               %#X ", Tx.st_hvsto_1.AVL_TEMP_HVSTO.physicalvalue());
    Trace("AVL_TEMP_HTEX_HVSTO          %#X ", Tx.st_hvsto_1.AVL_TEMP_HTEX_HVSTO.physicalvalue());
    Trace("AVL_TEMP_HVSTO_MIN           %#X ", Tx.st_hvsto_1.AVL_TEMP_HVSTO_MIN.physicalvalue());
    Trace("AVL_TEMP_HVSTO_MAX           %#X ", Tx.st_hvsto_1.AVL_TEMP_HVSTO_MAX.physicalvalue());
    Trace("ST_WARN_OTMP_HVSTO           %#X ", Tx.st_hvsto_1.ST_WARN_OTMP_HVSTO.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.dt_hvsto_2");
    Trace("SYNCN_DT_HVSTO_2             %#X ", Tx.dt_hvsto_2.SYNCN_DT_HVSTO_2.physicalvalue());
    Trace("MUX_DT_HVSTO_2               %#X ", Tx.dt_hvsto_2.MUX_DT_HVSTO_2.physicalvalue());
    Trace("PRD_ENERG_CHGCOND_2_COS      %#X ", Tx.dt_hvsto_2.PRD_ENERG_CHGCOND_2_COS.physicalvalue());
    Trace("PRD_ENERG_CHGTAR_2_HVSTO     %#X ", Tx.dt_hvsto_2.PRD_ENERG_CHGTAR_2_HVSTO.physicalvalue());
    Trace("AVL_ISRE                     %#X ", Tx.dt_hvsto_2.AVL_ISRE.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.stat_hvsto_vrfd");
    Trace("CRC_ST_HVSTO_VRFD            %#X ", Tx.stat_hvsto_vrfd.CRC_ST_HVSTO_VRFD.physicalvalue());
    Trace("ALIV_ST_HVSTO_VRFD           %#X ", Tx.stat_hvsto_vrfd.ALIV_ST_HVSTO_VRFD.physicalvalue());
    Trace("AVL_I_HVSTO_VRFD             %#X ", Tx.stat_hvsto_vrfd.AVL_I_HVSTO_VRFD.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.lim_chg_dchg_hvsto");
    Trace("U_MAX_CHG_HVSTO              %#X ", Tx.lim_chg_dchg_hvsto.U_MAX_CHG_HVSTO.physicalvalue());
    Trace("I_DYN_MAX_CHG_HVSTO          %#X ", Tx.lim_chg_dchg_hvsto.I_DYN_MAX_CHG_HVSTO.physicalvalue());
    Trace("U_MIN_DCHG_HVSTO             %#X ", Tx.lim_chg_dchg_hvsto.U_MIN_DCHG_HVSTO.physicalvalue());
    Trace("I_DYN_MAX_DCHG_HVSTO         %#X ", Tx.lim_chg_dchg_hvsto.I_DYN_MAX_DCHG_HVSTO.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.ident_hvsto");
    Trace("IDENT_HVSTO_SIG              %#X ", Tx.ident_hvsto.IDENT_HVSTO_SIG.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.svc_sme");
    Trace("ID2                          %#X ", Tx.svc_sme.ID2.physicalvalue());
    Trace("ID_FN_RQ_BUS                 %#X ", Tx.svc_sme.ID_FN_RQ_BUS.physicalvalue());
    Trace("ID_RQ_BUS                    %#X ", Tx.svc_sme.ID_RQ_BUS.physicalvalue());
    Trace("ST_RQ_BUS                    %#X ", Tx.svc_sme.ST_RQ_BUS.physicalvalue());
    Trace("ST_LAW_RQ                    %#X ", Tx.svc_sme.ST_LAW_RQ.physicalvalue());
    Trace("DUR_FLLUPT                   %#X ", Tx.svc_sme.DUR_FLLUPT.physicalvalue());
    Trace("NO_CC_MESS_STD               %#X ", Tx.svc_sme.NO_CC_MESS_STD.physicalvalue());
    Trace("ST_CC_MESS_STD               %#X ", Tx.svc_sme.ST_CC_MESS_STD.physicalvalue());
    Trace("TRANF_CC_MESS_STD            %#X ", Tx.svc_sme.TRANF_CC_MESS_STD.physicalvalue());
    Trace("ST_IDC_CCLK_CC_MESS_STD      %#X ", Tx.svc_sme.ST_IDC_CCLK_CC_MESS_STD.physicalvalue());
    Trace("NO_CC_MESS_EXT               %#X ", Tx.svc_sme.NO_CC_MESS_EXT.physicalvalue());
    Trace("ST_CC_MESS_EXT               %#X ", Tx.svc_sme.ST_CC_MESS_EXT.physicalvalue());
    Trace("TRANF_CC_MESS_EXT            %#X ", Tx.svc_sme.TRANF_CC_MESS_EXT.physicalvalue());
    Trace("ST_IDC_CCLK_CC_MESS_EXT      %#X ", Tx.svc_sme.ST_IDC_CCLK_CC_MESS_EXT.physicalvalue());
    Trace("NO_FRM_CC_MESS_EXT           %#X ", Tx.svc_sme.NO_FRM_CC_MESS_EXT.physicalvalue());
    Trace("QUAN_FRM_CC_MESS_EXT         %#X ", Tx.svc_sme.QUAN_FRM_CC_MESS_EXT.physicalvalue());
    Trace("UTDT_CC_MESS                 %#X ", Tx.svc_sme.UTDT_CC_MESS.physicalvalue());
    Trace("INQY_ID_PWRCOS_FN_1          %#X ", Tx.svc_sme.INQY_ID_PWRCOS_FN_1.physicalvalue());
    Trace("INQY_PWR_LV_FN_1             %#X ", Tx.svc_sme.INQY_PWR_LV_FN_1.physicalvalue());
    Trace("INQY_PWR_HV_FN_1             %#X ", Tx.svc_sme.INQY_PWR_HV_FN_1.physicalvalue());
    Trace("INQY_PWR_HV_FN_2             %#X ", Tx.svc_sme.INQY_PWR_HV_FN_2.physicalvalue());
    Trace("INQY_PWR_LV_FN_2             %#X ", Tx.svc_sme.INQY_PWR_LV_FN_2.physicalvalue());
    Trace("INQY_ID_PWRCOS_FN_2          %#X ", Tx.svc_sme.INQY_ID_PWRCOS_FN_2.physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.st_diag_obd_2_pt");
    Trace("ALIV_ST_DIAG_OBD_2_PT        %#X ", Tx.st_diag_obd_2_pt.ALIV_ST_DIAG_OBD_2_PT.physicalvalue());
    Trace("ST_DIAG_OBD_2_PT_MAX_MUX     %#X ", Tx.st_diag_obd_2_pt.ST_DIAG_OBD_2_PT_MAX_MUX.physicalvalue());
    Trace("ST_DIAG_OBD_2_PT_IMME_MUX    %#X ", Tx.st_diag_obd_2_pt.ST_DIAG_OBD_2_PT_IMME_MUX.physicalvalue());
    Trace("DIAG_ST_OBD_2_PT_1           %#X ", Tx.st_diag_obd_2_pt.DIAG_ST_OBD_2_PT_1.physicalvalue());
    Trace("DIAG_ST_OBD_2_PT_2           %#X ", Tx.st_diag_obd_2_pt.DIAG_ST_OBD_2_PT_2.physicalvalue());
    Trace("DIAG_ST_OBD_2_PT_3           %#X ", Tx.st_diag_obd_2_pt.DIAG_ST_OBD_2_PT_3.physicalvalue());
    Trace("DIAG_ST_OBD_2_PT_4           %#X ", Tx.st_diag_obd_2_pt.DIAG_ST_OBD_2_PT_4.physicalvalue());
    Trace("DIAG_ST_OBD_2_PT_5           %#X ", Tx.st_diag_obd_2_pt.DIAG_ST_OBD_2_PT_5.physicalvalue());
    Trace("DIAG_ST_OBD_2_PT_6           %#X ", Tx.st_diag_obd_2_pt.DIAG_ST_OBD_2_PT_6.physicalvalue());
    Trace("DIAG_ST_OBD_2_PT_7           %#X ", Tx.st_diag_obd_2_pt.DIAG_ST_OBD_2_PT_7.physicalvalue());
    Trace("DIAG_ST_OBD_2_PT_8           %#X ", Tx.st_diag_obd_2_pt.DIAG_ST_OBD_2_PT_8 .physicalvalue());
    Trace("------------------------------------------------------");
    Trace( "%s", "Tx.nm2_a_can_sme");
    Trace("NM2_AUTOSAR_RES_ACAN         %#X ", Tx.nm2_a_can_sme.NM2_AUTOSAR_RES_ACAN.physicalvalue());
    Trace("NM2_USR_DT_ACAN              %#X ", Tx.nm2_a_can_sme.NM2_USR_DT_ACAN.physicalvalue());

}